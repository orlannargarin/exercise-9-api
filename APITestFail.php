<?php
require_once 'API.php';
require_once('RequestHandler.php');
use PHPUnit\Framework\TestCase;

class APITestFail extends TestCase
{
    private $api;

    protected function setUp(): void
    {
        $this->api = new API();
    }

    public function testPayload(): array
    {
        $payload = array(
            'forPost' => null,
            'forGet' => 1,
            'forPutId' => [
                'id' => 1
            ],
            'forPutPayload' => [
                'id' => 13,
                'first_name' => "Testasdd",
                'middle_name' => "M2",
                'last_name' => "Test Last2asd",
                'contact_number' => "86931664824",
            ],
            'forDeleteId' => [
                'id' => null
            ],
            'forDeletePayload' => [
                'id' => 99999
            ]
        );

        $load = $payload;
        $this->assertNotEmpty($load);
        return $load;
    }

    /**
     * @depends testPayload
     * 
     */
    public function testHttpPost(array $load)
    {
        $_SERVER['REQUEST_METHOD'] = 'POST';

        $filter = $load['forPost'];

        // Capture the JSON response
        ob_start();
        $this->api->httpPost($filter);
        $response = ob_get_clean();

        // Decode the JSON response to an array
        $result = json_decode($response, true);

        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'failed');
        $this->assertArrayHasKey('message', $result);
    }

    /**
     * @depends testPayload
     * 
     */
    public function testHttpGet(array $load)
    {
        $_SERVER['REQUEST_METHOD'] = 'GET';

        $filter = $load['forGet'];

        // Capture the JSON response
        ob_start();
        $this->api->httpGet($filter);
        $response = ob_get_clean();

        // Decode the JSON response to an array
        $result = json_decode($response, true);

        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'failed');
        $this->assertArrayHasKey('message', $result);
    }

    /**
     * @depends testPayload
     * 
     */
    public function testHttpPut(array $load)
    {
        $_SERVER['REQUEST_METHOD'] = 'PUT';

        $filterId = $load['forPutId']['id'];
        $filterPayload = $load['forPutPayload'];

        // Capture the JSON response
        ob_start();
        $this->api->httpPut($filterId, $filterPayload);
        // $this->api->httpPut($id, $payload);
        $response = ob_get_clean();

        // Decode the JSON response to an array
        $result = json_decode($response, true);

        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'failed');
        $this->assertArrayHasKey('message', $result);
    }


    /**
     * @depends testPayload
     * 
     */
    public function testHttpDelete(array $load)
    {
        $_SERVER['REQUEST_METHOD'] = 'DELETE';

        $filterid = $load['forDeleteId']['id'];
        $filterPayload = $load['forDeletePayload'];


        // Capture the JSON response
        ob_start();
        $this->api->httpDelete($filterid, $filterPayload);
        $response = ob_get_clean();

        // Decode the JSON response to an array
        $result = json_decode($response, true);

        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'failed');
        $this->assertArrayHasKey('message', $result);
    }
}

?>