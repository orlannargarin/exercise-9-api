<?php
class RequestHandler
{
    public static function getRequestMethod()
    {
        return $_SERVER['REQUEST_METHOD'] ?? '';
    }

    public static function getRequestData()
    {
        if (self::getRequestMethod() === 'GET') {
            return $_GET;
        } else {
            $requestBody = file_get_contents('php://input');
            return json_decode($requestBody, true) ?? [];
        }
    }
}
?>