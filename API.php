<?php
// Add headers to allow for cross-origin resource sharing (CORS)
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Methods: POST, GET, PUT, DELETE");
header("Access-Control-Allow-Headers: Content-Type");

require_once('C:\xampp\htdocs\Internship\exercise-9-api\MysqliDb.php');

class API
{
    private $db;
    public function __construct()
    {
        $this->db = new MysqliDb('localhost', 'root', '', 'employee');
    }

    /**
     * HTTP GET Request
     *
     * @param $payload
     */
    public function httpGet($payload)
    {
        if (!is_array($payload)) {
            $this->failedResponse("Invalid Payload");
            return;
        }

        // Get the 'id' from the payload
        $id = $payload['id'];

        $data = $this->db->where('id', $id)->get('information');

        if ($this->db->count > 0) {
            $this->successResponse($data);
        } else {
            $this->failedResponse("Failed Fetch Request");
        }
    }

    /**
     * HTTP POST Request
     *
     * @param $payload
     */
    public function httpPost($payload)
    {
        if (!is_array($payload) || empty($payload)) {
            $this->failedResponse("Invalid Payload");
            return;
        }

        $insertId = $this->db->insert('information', $payload);

        if ($insertId) {
            $insertedData = $this->db->where('id', $insertId)->getOne('information');
            $this->successResponse($insertedData);
        } else {
            $this->failedResponse("Failed to Insert Data");
        }
    }

    /**
     * HTTP PUT Request
     *
     * @param $id
     * @param $payload
     */
    public function httpPut($id, $payload)
    {
        if (empty($id) || empty($payload) || $id != $payload['id']) {
            $this->failedResponse("Invalid ID or Payload (ID:" . $id . ", Payload ID: " . $payload['id'] . ")");
            return;
        }
        $result = $this->db->where('id', $id)->update('information', $payload);

        if ($result) {
            $this->successResponse("Data updated successfully");
        } else {
            $this->failedResponse("Failed to Update Data");
        }
    }

    /**
     * HTTP DELETE Request
     *
     * @param $id
     * @param $payload
     */
    public function httpDelete($id, $payload)
    {
        if (empty($id) || empty($payload) || $id != $payload['id']) {
            $this->failedResponse("Invalid ID or Payload (ID:" . $id . ", Payload ID: " . $payload['id'] . ")");
            return;
        }

        if (is_array($payload)) {
            // Use the where() function for a single ID
            $this->db->where('id', $id);

            // Execute the delete query
            $result = $this->db->delete('information');
        }


        if ($result) {
            $this->successResponse("DELETE request successful");
        } else {
            $this->failedResponse("Failed to Delete Data");
        }
    }

    private function successResponse($data)
    {
        echo json_encode(
            array(
                'method' => $_SERVER['REQUEST_METHOD'],
                'status' => 'success',
                'data' => $data,
            )
        );
    }

    private function failedResponse($message)
    {
        echo json_encode(
            array(
                'method' => $_SERVER['REQUEST_METHOD'],
                'status' => 'failed',
                'message' => $message,
            )
        );
    }
}

// // Identifier for the request method
// $request_method = $_SERVER['REQUEST_METHOD'];

// // For GET, POST, PUT & DELETE Request
// if ($request_method === 'GET') {
//     $received_data = $_GET;
// } else {
//     if ($request_method === 'PUT' || $request_method === 'DELETE') {
//         $request_uri = $_SERVER['REQUEST_URI'];

//         $ids = null;
//         $exploded_request_uri = array_values(array_filter(explode("/", $request_uri)));
//         $last_index = count($exploded_request_uri) - 1;
//         $ids = $exploded_request_uri[$last_index];
//     }

//     // Payload data
//     $received_data = json_decode(file_get_contents('php://input'), true);
// }

// // Create an instance of the API class
// $api = new API;

// // Checking the request method and calling the appropriate function
// switch ($request_method) {
//     case 'GET':
//         $api->httpGet($received_data);
//         break;
//     case 'POST':
//         $api->httpPost($received_data);
//         break;
//     case 'PUT':
//         $api->httpPut($ids, $received_data);
//         break;
//     case 'DELETE':
//         $api->httpDelete($ids, $received_data);
//         break;
// }


?>